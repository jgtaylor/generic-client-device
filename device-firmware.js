var server = "192.168.0.0",	SSID = "XXXX",	ssidPassword = "XXXXXXX", wsPath = "/devices/XXXX", wsPort = 1880;
var w = require( "Wifi" );
var WebSocket = require( "ws" );
var WebSock = {};
var configMap = [
  {
		id: "one",
		pin: D13,
		type: "button",
		validCmds: [ "on", "off", "toggle", "getState" ],
		meta: {
			usage: "Mains Relay"
		}
	}
];

function configGen( config ) {
  let ret = [];
  config.forEach( ( el ) => {
    let t = {
      device: el.id,
      type: el.type,
      validCmds: el.validCmds,
      meta: el.meta
    };
    ret.push( t );
  } );
  return ret;
}

function button( d, cmd ) {
  if ( d.pin.getMode() !== "output" ) {
    d.pin.mode( "output" );
  }
  switch ( cmd ) {
    case "on":
    {
      digitalWrite( d.pin, 1 );
      let retMsg = ["state", {
        device: d.id,
        mode: d.pin.getMode(),
        value: d.pin.read()
      }];
      WebSock.send( JSON.stringify( retMsg ) );
      break;
    }
    case "off":
    {
      digitalWrite( d.pin, 0 );
      let retMsg = ["state", {
        device: d.id,
        mode: d.pin.getMode(),
        value: d.pin.read()
      }];
      WebSock.send( JSON.stringify( retMsg ) );
      break;
    }
    case "getState":
    {
      WebSock.send( JSON.stringify( ["state", {
        device: d.id,
        mode: d.pin.getMode(),
        value: d.pin.read()
      }] ) );
      break;
    }
    case "toggle":
    {
      digitalWrite( d.pin, 1 );
      setTimeout( function() {
          digitalWrite(d.pin, 0);
          WebSock.send( JSON.stringify( ["state",
          {
            device: d.id,
            mode: d.pin.getMode(),
            value: d.pin.read()
          }] ) );
      }, 50);
      let retMsg = ["state", {
        device: d.id,
        mode: d.pin.getMode(),
        value: d.pin.read()
      }];
      WebSock.send( JSON.stringify( retMsg ) );
      break;
    }
    default:
    {
      break;
    }
  }
}

function dimmer( d, cmd ) {
  switch ( cmd ) {
    case "read":
      {
        WebSock.send( JSON.stringify( ["reading", {
          device: d.id,
          value: analogRead()
        }] ) );
      }
      break;
    default:
      analogRead();
      break;

  }
}

function virtual( d, cmd ) {
  d.pin( cmd );
}

function msgParse( msg ) {
  var m = JSON.parse( msg );

  function device( map ) {
    for ( let x = 0; x < map.length; x++ ) {
      if ( map[x].id === m[1].device ) {
        return map[x];
      }
    }
    return {
      id: null,
      pin: null,
      type: null,
      validCmds: null,
      meta: {
        keys: [{
          name: null,
          metric: null,
          unit: null
        }],
        deviceName: null
      }
    };
  }
  switch ( m[0] ) {
    case "cmd":
    {
      let d = device( configMap );
      switch ( d.type ) {
        case "button":
        {
          button( d, m[1].cmd );
          break;
        }
        case "virtual":
        {
          virtual( d, m[1].cmd );
          break;
        }
        case "dimmer":
        {
          dimmer( d, m[1].cmd );
          break;
        }
        default:
          break;
      }
      break;
    }
    case "config":
    {
      WebSock.send( JSON.stringify( ["config", configGen( configMap )] ) );
      break;
    }
    default:
      break;
  }
}

function WebSockconnect( state ) {
  if ( state === 1 ) {
    WebSock.removeAllListeners();
    WebSock = null;
  }
  WebSock = new WebSocket( server, {
    path: wsPath,
    port: wsPort,
    origin: "MCU",
    keepAlive: 60
  } );
  WebSock.on( "error", ( err ) => {
    console.log( "ERROR: Websocket error: ", err );
  } );
  WebSock.on( "open", () => {
    WebSock.send( JSON.stringify( ["config", configGen( configMap )] ) );
  } );
  WebSock.on( "close", () => {
    setTimeout( function () {
      WebSockconnect( 1 );
    }, 10000 ); // 10 seconds between reconnect attempts.
    console.log( "ERROR: Websocket disconnected" );
  } );
  WebSock.on( "message", ( msg ) => {
    msgParse( msg.toString() );
  } );

}

E.on( "init", () => {
  w.stopAP();
  w.on( "disconnected", () => {
    w.connect( SSID, {
      password: ssidPassword
    }, function ( error ) {
      console.log( error );
      w.startAP();
    } );
  } );
  w.on( "connected", function () {
    WebSockconnect( 0 );
  } );

  w.connect( SSID, {
    password: ssidPassword
  }, ( error ) => {
    if ( error ) {
      console.log( error );
      w.startAP();
    }
  } );
} );